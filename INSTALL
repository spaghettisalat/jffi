You need an ECL instance with 32bit wide characters and :int64-t support
(this is not enabled by default for android!) to use JFFI.

There are two ways to build JFFI: A make-based one and one using ASDF

make build
----------
 Open up a shell and type in

 make ECL_TOPDIR=/path/to/your/ecl/installation

 Additional make flags are
 LIBJVMDIR: directory, in which the libjvm.so library is installed
 (this location is hardcoded in jffi.fas by default. Remove the
 -rpath options in compile-fasl.lisp to disable this)
 JVMINCLUDEDIR: the JVM include directory, where the jni.h file is located

 The default make target builds JFFI with 64bit pointers. If you only
 need 32bit, invoke make with the 32bit target:
 'make ECL_TOPDIR=/path/to/your/ecl/installation 32bit'

ASDF build
---------
 Start your Lisp instance and type in:

 (asdf:load-system "jffi")

 All output files are written to the ASDF output cache, usually located under ~/.cache/common-lisp/
 To disable this, type

 (asdf:disable-output-translations)

 Additional option keys are:
 :pointersize --- size of a pointer, either :32bit or :64bit
 :ecl-includedir --- a string, directory of the ECL include files
 :ecl-libdir --- a string, path to the libecl.so shared library
 :jvm-includedir --- a string, directory of the JVM include files
 :libjvmdir --- a string, directory where the libjvm.so shared library is located
 :cflags --- a string, additional compiler flags
 :ldflags --- a string, additional linker flags

 To use this option keys, you have to pass them to the asdf:compile-system or asdf:load-system function, e.g.:

 (asdf:compile-system "jffi" :pointersize :32bit :jvm-includedir "/usr/lib/java-8-openjdk-i386/include/")