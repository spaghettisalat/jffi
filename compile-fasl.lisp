(setq c::*ld-flags* (si:getenv "LDFLAGS")
      c::*cc-flags* (si:getenv "CFLAGS")
      c::*ecl-include-directory* (si:getenv "ECL_INCLUDEDIR")
      c::*ecl-library-directory* (si:getenv "ECL_LIBDIR"))
(require 'cmp)
(c:build-fasl "jffi" :lisp-files '("jffi.o" "jffi_c_layer.o" "jffi_Java_start.o")
              :ld-flags `(,(concatenate 'string
                                        "-L" (si:getenv "LIBJVMDIR"))
                           ,(concatenate 'string
                                         "-L" (si:getenv "ECL_LIBDIR"))
                           "-ljvm"
                           ,(concatenate
                             'string
                             "-Wl,-rpath," (si:getenv "LIBJVMDIR"))))
