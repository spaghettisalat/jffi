(setq c::*ld-flags* (si:getenv "LDFLAGS")
      c::*cc-flags* (si:getenv "CFLAGS")
      c::*ecl-include-directory* (si:getenv "ECL_INCLUDEDIR")
      c::*ecl-library-directory* (si:getenv "ECL_LIBDIR"))
(if (not
     (and
      (compile-file "../jffi.lisp" :system-p t)
      (compile-file "lisp-test.lisp" :system-p t)))
    (progn (format t "~%") (quit -1)))
(c:build-static-library "tests"
                        :lisp-files '("lisp-test.o")
                        :init-name "TESTS")
