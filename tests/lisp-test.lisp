;; Copyright 2017 Marius Gerbershagen
;; This file is part of JFFI.
;;
;;  JFFI is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU Lesser General Public License as
;;  published by the Free Software Foundation, either version 3 of the
;;  License, or (at your option) any later version.
;;
;;  JFFI is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU Lesser General Public License for more details.
;;
;;  You should have received a copy of the GNU Lesser General Public
;;  License along with JFFI.  If not, see <http://www.gnu.org/licenses/>.

(defpackage #:lisp-test (:use #:common-lisp))
(in-package #:lisp-test)
(jffi:def-class "" "test_class"
  :fields (("boolean_field" :type :boolean)
           ("byte_field" :type :byte)
           ("char_field" :type :char)
           ("short_field" :type :short)
           ("int_field" :type :int)
           ("long_field" :type :long)
           ("float_field" :type :float)
           ("double_field" :type :double)
           ("string_field" :type "java/lang/String")
           ("boolean_field_static" :type :boolean :allocation :static)
           ("byte_field_static" :type :byte :allocation :static)
           ("char_field_static" :type :char :allocation :static)
           ("short_field_static" :type :short :allocation :static)
           ("int_field_static" :type :int :allocation :static)
           ("long_field_static" :type :long :allocation :static)
           ("float_field_static" :type :float :allocation :static)
           ("double_field_static" :type :double :allocation :static)
           ("string_field_static" :type "java/lang/String" :allocation :static))
  :methods (("boolean_method" ((x :boolean)) :returning :boolean)
            ("byte_method" ((x :byte)) :returning :byte)
            ("char_method" ((x :char)) :returning :char)
            ("short_method" ((x :short)) :returning :short)
            ("int_method" ((x :int)) :returning :int)
            ("long_method" ((x :long)) :returning :long)
            ("float_method" ((x :float)) :returning :float)
            ("double_method" ((x :double)) :returning :double)
            ("string_method" ((x "java/lang/String")) :returning "java/lang/String")
            ("boolean_method_static" ((x :boolean)) :returning :boolean :allocation :static)
            ("byte_method_static" ((x :byte)) :returning :byte :allocation :static)
            ("char_method_static" ((x :char)) :returning :char :allocation :static)
            ("short_method_static" ((x :short)) :returning :short :allocation :static)
            ("int_method_static" ((x :int)) :returning :int :allocation :static)
            ("long_method_static" ((x :long)) :returning :long :allocation :static)
            ("float_method_static" ((x :float)) :returning :float :allocation :static)
            ("double_method_static" ((x :double)) :returning :double :allocation :static)
            ("string_method_static" ((x "java/lang/String")) :returning "java/lang/String" :allocation :static)
            ("exception_method" () :allocation :static)
            (("speed_test_method" speed-test-method-global) ((n :int)) :returning "java/util/LinkedList" :allocation :static :finalizer nil)
            (("speed_test_method" speed-test-method-local) ((n :int)) :returning "java/util/LinkedList" :allocation :static :finalizer :local))
  :constructors ((init ())))
(jffi:def-class "java/lang" "String"
  :methods (("equals" ((obj "java/lang/Object")) :returning :boolean)))

(defun fields-test (obj)
  (let ((obj2 (test-class.init)))
    (loop for i in '("boolean" "byte" "char" "short" "int" "long" "float"
                     "double")
       do (if (not (eql
                    (funcall
                     (intern (string-upcase (concatenate 'string
                                                         "test-class."
                                                         i
                                                         "-field"))
                             *package*)
                     obj)
                    (funcall
                     (intern (string-upcase (concatenate 'string
                                                         "test-class."
                                                         i
                                                         "-field"))
                             *package*)
                     obj2)))
              (return-from fields-test (format nil "Error in equality test for instance fields of type ~a!" i)))
       do (if (not (eql
                    (funcall
                     (intern (string-upcase (concatenate 'string
                                                         "test-class."
                                                         i
                                                         "-field-static"))
                             *package*))
                    (funcall
                     (intern (string-upcase (concatenate 'string
                                                         "test-class."
                                                         i
                                                         "-field"))
                             *package*)
                     obj2)))
              (return-from fields-test (format nil "Error in equality test for static fields of type ~a!" i))))
    (setf (test-class.int-field-static) 23)
    (setf (test-class.int-field obj2) 23)
    (if (not (and (eql (test-class.int-field-static) 23)
                  (eql (test-class.int-field obj2) 23)
                  (eql (test-class.int-field-static)
                       (test-class.int-field obj2))))
        (return-from fields-test "Error in setf test for fields of type int!"))
    (if (not (String.equals (test-class.string-field obj)
                            (test-class.string-field obj2)))
        (return-from fields-test (format nil "Error in equality test for instance fields of type String!")))
    (if (not (String.equals (test-class.string-field-static)
                            (test-class.string-field obj2)))
        (return-from fields-test (format nil "Error in equality test for static fields of type String!")))
    nil))

(defun methods-test (obj)
  (let ((obj2 (test-class.init)))
    (loop for i in '("boolean" "byte" "char" "short" "int" "long" "float"
                     "double")
       do (if (not (eql
                    (funcall
                     (intern (string-upcase (concatenate 'string
                                                         "test-class."
                                                         i
                                                         "-method"))
                             *package*)
                     obj
                     (funcall
                      (intern (string-upcase (concatenate 'string
                                                          "test-class."
                                                          i
                                                          "-field-static"))
                              *package*)))
                    (funcall
                     (intern (string-upcase (concatenate 'string
                                                         "test-class."
                                                         i
                                                         "-field"))
                             *package*)
                     obj2)))
              (return-from methods-test (format nil "Error in equality test for instance methods of type ~a!" i)))
       do (if (not (eql
                    (funcall
                     (intern (string-upcase (concatenate 'string
                                                         "test-class."
                                                         i
                                                         "-method-static"))
                             *package*)
                     (funcall
                      (intern (string-upcase (concatenate 'string
                                                          "test-class."
                                                          i
                                                          "-field-static"))
                              *package*)))
                    (funcall
                     (intern (string-upcase (concatenate 'string
                                                         "test-class."
                                                         i
                                                         "-field"))
                             *package*)
                     obj2)))
              (return-from methods-test (format nil "Error in equality test for static methods of type ~a!" i))))
    (if (not (String.equals (test-class.string-method obj (test-class.string-field obj2))
                            (test-class.string-field obj2)))
        (return-from methods-test (format nil "Error in equality test for instance methods of type String!")))
    (if (not (String.equals (test-class.string-method-static (test-class.string-field obj2))
                            (test-class.string-field obj2)))
        (return-from methods-test (format nil "Error in equality test for static methods of type String!")))
    (if (not (String.equals (test-class.string-field obj2)
                            (jffi:convert-to-jstring (jffi:convert-from-jstring (test-class.string-field obj2)))))
        (return-from methods-test (format nil "Error in equality test for (jffi:convert-to/from-jstring)!")))))

(defun exception-test ()
  (handler-case
      (progn (test-class.exception-method)
             (format nil "Error in exception test: No exception found where one was expected!"))
    (jffi:java-exception (c)
      (if (not (equal (jffi:java-exception-message c) "This is an exception test"))
          (format nil "Error in exception test: unexpected exception!")))))

(defun instanceof-test (obj)
  (progn (si::gc t)
         (if (not (jffi:instanceof obj "test_class"))
             "Error in instanceof-test for (instanceof obj \"test_class\")"
             (if (not (jffi:instanceof obj *test-class*))
                 "Error in instanceof-test for (instanceof obj *test-class*)"))))
(define-condition test-condition (error)
  ((message :initarg :message)))
(defun condition-test ()
  (error 'test-condition :message "This is a test condition"))
(defvar *finalizer-test-object* nil)
(defun finalizer-test-object ()
  (progn
    (setq *finalizer-test-object* (make-instance 'jffi::java-class :name "testabc" :fieldIDs (vector nil nil)))
    *finalizer-test-object*))
(defun finalizer-test ()
  (progn
    (format t "number of referenced lisp objects: ~a~%" (length jffi::*java-references*))
    (if (find *finalizer-test-object* jffi::*java-references* :test #'eq)
        t
        nil)))
(defun unref-test (obj)
  (progn (jffi:unref (test-class.string-field obj))
         (if (not (String.equals (test-class.string-method obj (test-class.string-field obj))
                                 (test-class.string-field obj)))
             "Error in unref-test for (unref obj)"
             (jffi:with-unref ((string (test-class.string-field obj)))
               (if (not (String.equals string
                                       (jffi:convert-to-jstring (jffi:convert-from-jstring (test-class.string-field obj)))))
                   "Error in unref test for (with-unref ...)"
                   nil)))))
(defun object-return-test ()
  (test-class.init))
(defvar *symbolvalue-testobject* 'abc)

(defun speed-test-global ()
  (time (progn
          (loop for i below 100000 do
               (let ((jobj (test-class.speed-test-method-global 1)))
                 (jffi:unref jobj)))
          (si::gc t))))

(defun speed-test-local ()
  (time (progn
          (loop for i below 100000 do
               (let ((jobj (test-class.speed-test-method-local 1)))
                 (jffi:unref-local jobj)))
          (si::gc t))))
