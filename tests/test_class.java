// Copyright 2017 Marius Gerbershagen
// This file is part of JFFI.
//
//  JFFI is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  JFFI is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with JFFI.  If not, see <http://www.gnu.org/licenses/>.

import java.util.LinkedList;

public class test_class{
	public boolean boolean_field;
	public byte byte_field;
	public char char_field;
	public short short_field;
	public int int_field;
	public long long_field;
	public float float_field;
	public double double_field;
	public String string_field;
	public static boolean boolean_field_static = true;
	public static byte byte_field_static = 12;
	public static char char_field_static = 'a';
	public static short short_field_static = 32767;
	public static int int_field_static = 32768;
	public static long long_field_static = 9000000000L;
	public static float float_field_static = 33.3f;
	public static double double_field_static = 33.3d;
	public static String string_field_static = "abc𝄞  𤽜";
	public test_class(){
		boolean_field = boolean_field_static;
		byte_field =  byte_field_static;
		char_field = char_field_static;
		short_field = short_field_static;
		int_field = int_field_static;
		long_field = long_field_static;
		float_field = float_field_static;
		double_field = double_field_static;
		string_field = string_field_static;
	}
	public boolean boolean_method(boolean x){
		return x;
	}
	public byte byte_method(byte x){
		return x;
	}
	public char char_method(char x){
		return x;
	}
	public short short_method(short x){
		return x;
	}
	public int int_method(int x){
		return x;
	}
	public long long_method(long x){
		return x;
	}
	public float float_method(float x){
		return x;
	}
	public double double_method(double x){
		return x;
	}
	public String string_method(String x){
		return x;
	}
	public static boolean boolean_method_static(boolean x){
		return boolean_field_static;
	}
	public static byte byte_method_static(byte x){
		return byte_field_static;
	}
	public static char char_method_static(char x){
		return char_field_static;
	}
	public static short short_method_static(short x){
		return short_field_static;
	}
	public static int int_method_static(int x){
		return int_field_static;
	}
	public static long long_method_static(long x){
		return long_field_static;
	}
	public static float float_method_static(float x){
		return float_field_static;
	}
	public static double double_method_static(double x){
		return double_field_static;
	}
	public static String string_method_static(String x){
		return string_field_static;
	}
	public static void exception_method() throws Exception{
		throw new Exception("This is an exception test");
	}

	public static LinkedList<Integer> speed_test_method(int n){
		LinkedList<Integer> ret = new LinkedList<Integer>();
		for(int i = 0; i < n; i++)
			ret.add(i);
		return ret;
	}
}
