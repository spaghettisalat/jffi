// Copyright 2017 Marius Gerbershagen
// This file is part of JFFI.
//
//  JFFI is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  JFFI is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with JFFI.  If not, see <http://www.gnu.org/licenses/>.

import jffi.*;
import java.util.LinkedList;

public class java_test{
	static{
		//System.loadLibrary("jffi");
		System.loadLibrary("lisp_init");
	}
	public static void main(String[] args){
		try{
			Lisp.start();
		}
		catch(LispEnvironmentError l){
			System.out.println("Failed test: can't start the lisp environment");
			System.exit(-2);
		}

		try{
			Thread t1 = new Thread(new Runnable(){
					public void run(){
						int errval = main_test();
						if(errval != 0){
							Lisp.attachCurrentThread();
							Lisp.stop();
							System.exit(errval);
						}
					}
				});
			t1.start();
			t1.join();
		}
		catch(InterruptedException i){}

		//speed_test();

		Lisp.stop();
	}
	public static int main_test(){
		try{
			Lisp.attachCurrentThread();
			System.out.println("thread attaching test passed");
			CLObject nil = Lisp.read("nil");
			CLObject t = Lisp.read("t");
			System.out.println("read test passed");
			if(!Lisp.funcall("EQ", nil, Lisp.NIL).toBoolean() || !Lisp.funcall("EQ", t, Lisp.T).toBoolean()){
				System.out.println("Failed NIL,T equality test: " + "(eq nil nil) = " + Lisp.funcall("EQ", nil, Lisp.NIL).toBoolean() + " (eq t t) = " + Lisp.funcall("EQ", t, Lisp.T).toBoolean());
				Lisp.stop();return -1;
			}
			System.out.println("NIL,T equality test passed");
			CLObject testsymbol1 = Lisp.read("testsym");
			CLObject testsymbol2 = Lisp.makeSymbol(Lisp.funcall("PACKAGE-NAME", Lisp.funcall("EVAL", Lisp.read("*PACKAGE*"))).toString(), "TESTSYM");
			if(!Lisp.funcall("EQL", testsymbol1, testsymbol2).toBoolean()){
				System.out.println("Failed makeSymbol test");
				Lisp.stop();return -1;
			}
			System.out.println("makeSymbol test passed");
			CLObject testkeyword1 = Lisp.read(":testkwd");
			CLObject testkeyword2 = Lisp.makeKeyword("testkwd");
			if(!Lisp.funcall("EQL", testkeyword1, testkeyword2).toBoolean()){
				System.out.println("Failed makeKeyword test");
				Lisp.stop();return -1;
			}
			System.out.println("makeKeyword test passed");
			CLObject symbolvalue_testobj1 = Lisp.symbolValue(Lisp.makeSymbol("lisp-test", "*symbolvalue-testobject*"));
			CLObject symbolvalue_testobj2 = Lisp.makeSymbol("lisp-test", "abc");
			if(!Lisp.funcall("EQL", symbolvalue_testobj1, symbolvalue_testobj2).toBoolean()){
				System.out.println("Failed symbolValue test");
				Lisp.stop();return -1;
			}
			System.out.println("symbolValue test passed");	 
			CLObject ret = Lisp.funcall("LISP-TEST", "FIELDS-TEST", new test_class());
			if(!Lisp.funcall("EQL", ret, nil).toBoolean()){
				System.out.println("Failed fields test: " + ret.toString());
				Lisp.stop();return -1;
			}
			System.out.println("fields test passed");
			ret = Lisp.funcall("LISP-TEST", "METHODS-TEST", new test_class());
			if(!Lisp.funcall("EQL", ret, nil).toBoolean()){
				System.out.println("Failed methods test: " + ret.toString());
				Lisp.stop();return -1;
			}
			System.out.println("methods test passed");
			//Try again to see if caching of jclass and jmethodIDs works
			ret = Lisp.funcall("LISP-TEST", "FIELDS-TEST", new test_class());
			if(!Lisp.funcall("EQL", ret, nil).toBoolean()){
				System.out.println("Failed fields caching test: " + ret.toString());
				Lisp.stop();return -1;
			}
			System.out.println("fields caching test passed");
			ret = Lisp.funcall("LISP-TEST", "METHODS-TEST", new test_class());
			if(!Lisp.funcall("EQL", ret, nil).toBoolean()){
				System.out.println("Failed methods caching test: " + ret.toString());
				Lisp.stop();return -1;
			}
			System.out.println("methods caching test passed");
			ret = Lisp.funcall("LISP-TEST", "EXCEPTION-TEST");
			if(!Lisp.funcall("EQL", ret, nil).toBoolean()){
				System.out.println("Failed exception test: " + ret.toString());
				Lisp.stop();return -1;
			}
			System.out.println("exception test passed");
			ret = Lisp.funcall("LISP-TEST", "INSTANCEOF-TEST", new test_class());
			if(!Lisp.funcall("EQL", ret, nil).toBoolean()){
				System.out.println("Failed instanceof test: " + ret.toString());
				Lisp.stop();return -1;
			}
			System.out.println("instanceof test passed");
			ret = Lisp.funcall("LISP-TEST", "UNREF-TEST", new test_class());
			if(!Lisp.funcall("EQL", ret, nil).toBoolean()){
				System.out.println("Failed unref test: " + ret.toString());
				Lisp.stop();return -1;
			}
			System.out.println("unref test passed");
			test_class test_obj = (test_class) Lisp.object_funcall("LISP-TEST", "OBJECT-RETURN-TEST");
			String eq_result = test_equality(test_obj);
			if(eq_result != null){
				System.out.println("object return test failed: " + eq_result);
				Lisp.stop();return -1;
			}
			System.out.println("object return test passed");
		}
		catch(LispCondition c){
			System.out.println("Failed test: unexpected lisp condition");
			System.out.println(c.toString());
			Lisp.stop();return -1;
		}
		catch(Throwable t){
			System.out.println("Failed test: unexpected java exception");
			System.out.println(t.toString());
			Lisp.stop();return -1;
		}
		boolean condition = false;
		try{
			Lisp.funcall("LISP-TEST", "CONDITION-TEST");
			condition = false;
		}
		catch(LispCondition c){
			if(!Lisp.funcall("SLOT-VALUE", c.getCondition(), Lisp.read("MESSAGE")).toString().equals("This is a test condition")){
				System.out.println("Failed condition test: unexpected condition");
				Lisp.stop();return -1;
			}
			condition = true;
		}
		catch(Throwable t){
			System.out.println("Failed test: unexpected java exception");
			System.out.println(t.toString());
			Lisp.stop();return -1;
		}
		if(!condition){
			System.out.println("Failed condition test: No condition found where one was expected");
			Lisp.stop();return -1;
		}
		System.out.println("condition test passed");
		int z;
		try{
			finalizer_test_method();
			LinkedList abc = new LinkedList();
			for(z = 0; z < 10; z++){
				System.runFinalization();
				System.gc();
				Thread.sleep(100);
				//-Xcheck:jni spits out false warnings from here, because it
				//thinks, that funcall_symbol is a static void java method.
				//One can safely ignore these warnings.
				if(!Lisp.funcall("LISP-TEST", "FINALIZER-TEST").toBoolean())
					break;
				//trying to force finalization by allocating a lot of memory
				for(int i = 0; i < 100000; i++)
					abc.add(new Integer(i));
			}
		}
		catch(LispCondition c){
			System.out.println("Failed finalize test: unexpected lisp condition");
			System.out.println(c.toString());
			Lisp.stop();return -1;
		}
		catch(Throwable t){
			System.out.println("Failed finalize test: unexpected java exception");
			System.out.println(t.toString());
			Lisp.stop();return -1;
		}
		if(z == 10){
			System.out.println("Failed finalize test: CLObject.finalizer() doesn't seem to be working");
			Lisp.stop();return -1;
		}
		System.out.println("finalizer test passed");
		Lisp.detachCurrentThread();
		System.out.println("All tests passed!");
		return 0;
	}
	public static void finalizer_test_method(){
		CLObject ftest = Lisp.funcall("LISP-TEST", "FINALIZER-TEST-OBJECT");
		System.out.println("finalizer_test_method: " + Lisp.funcall("SLOT-VALUE", ftest, Lisp.read("JFFI::NAME")).toString());
	}
	public static String test_equality(test_class obj){
		test_class obj2 = new test_class();
		if(obj.boolean_field != obj2.boolean_field)
			return "Equality test for booleans failed";
		if(obj.byte_field != obj2.byte_field)
			return "Equality test for bytes failed";
		if(obj.char_field != obj2.char_field)
			return "Equality test for chars failed";
		if(obj.short_field != obj2.short_field)
			return "Equality test for shorts failed";
		if(obj.int_field != obj2.int_field)
			return "Equality test for ints failed";
		if(obj.long_field != obj2.long_field)
			return "Equality test for longs failed";
		if(obj.float_field != obj2.float_field)
			return "Equality test for floats failed";
		if(obj.double_field != obj2.double_field)
			return "Equality test for doubles failed";
		if(!obj.string_field.equals(obj2.string_field))
			return "Equality test for strings failed";
		if(obj.boolean_field_static != obj2.boolean_field_static)
			return "Equality test for booleans failed";
		if(obj.byte_field_static != obj2.byte_field_static)
			return "Equality test for bytes failed";
		if(obj.char_field_static != obj2.char_field_static)
			return "Equality test for chars failed";
		if(obj.short_field_static != obj2.short_field_static)
			return "Equality test for shorts failed";
		if(obj.int_field_static != obj2.int_field_static)
			return "Equality test for ints failed";
		if(obj.long_field_static != obj2.long_field_static)
			return "Equality test for longs failed";
		if(obj.float_field_static != obj2.float_field_static)
			return "Equality test for floats failed";
		if(obj.double_field_static != obj2.double_field_static)
			return "Equality test for doubles failed";
		if(!obj.string_field_static.equals(obj2.string_field_static))
			return "Equality test for strings failed";
		return null;
	}

	public static void speed_test(){
		System.out.println("speed test for local references:");
		for(int i = 0; i < 5; i++)
			Lisp.funcall("LISP-TEST", "SPEED-TEST-LOCAL");
		System.out.println("speed test for global references:");
		for(int i = 0; i < 5; i++)
			Lisp.funcall("LISP-TEST", "SPEED-TEST-GLOBAL");
	}
}
