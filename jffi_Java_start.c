// Copyright 2017 Marius Gerbershagen
// This file is part of JFFI.
//
//  JFFI is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  JFFI is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with JFFI.  If not, see <http://www.gnu.org/licenses/>.

#include <stdlib.h>
#include "jffi_c_layer.h"

jint java_create_java_vm(jint n_options, char** options){
	JavaVMInitArgs initargs;
	JavaVMOption* initoptions = malloc((1 + n_options) * sizeof(JavaVMOption));
	if(initoptions == NULL)
		return -1;
	//-Xrs disables JVM signal handlers
	initoptions[0].optionString = "-Xrs";
	for(int i = 1; i <= n_options; i++)
		initoptions[i].optionString = options[i - 1];
	initargs.version = JNI_VERSION_1_2;
	initargs.options = initoptions;
	initargs.nOptions = n_options + 1;
	initargs.ignoreUnrecognized = TRUE;
	jint error_code = JNI_CreateJavaVM(&java_vm, (void**) &java_env, &initargs);
	free(initoptions);
	if(error_code != JNI_OK)
		return error_code;
	pseudo_start = 1;
	attached_to_lisp = 1;
	return JNI_OK;
}
