(defclass java-package (module) ())

(defsystem "jffi" 
    :description "Fast, lightweight, bidirectional Java-Lisp foreign function interface using Embeddable Common Lisp and the Java Native Interface"
    :version "1.2.1"
    :author "Marius Gerbershagen <marius.gerbershagen@gmail.com>"
    :licence "LGPLv3"
    :in-order-to ((test-op (test-op "jffi/test")))
    :components
    ((:file "jffi" :depends-on ("jffi_c_layer"
                                "jffi_Java_start"
                                "jffi_Java_pseudo_start"
                                "jffi_Lisp_start")
            :perform (compile-op (o c)
                                 (setq c::*ld-flags* *ldflags*
                                       c::*cc-flags* (concatenate 'string
                                                                  *cflags*
                                                                  " -I" *jvm-includedir*
                                                                  " -I" *jvm-includedir* "/linux"
                                                                  " -fPIC")
                                       c::*ecl-include-directory* *ecl-includedir*
                                       c::*ecl-library-directory* *ecl-libdir*)
                                 (compile-file "jffi.lisp" :system-p t)
                                 (c:build-static-library "jffi_lisp"
                                                         :lisp-files '("jffi.o")
                                                         :init-name "JFFI_LISP")
                                 (c:build-fasl "jffi" :lisp-files (list "jffi.o"
                                                                        (namestring (apply-output-translations "jffi_c_layer.o"))
                                                                        (namestring (apply-output-translations "jffi_Java_start.o")))
                                               :ld-flags `(,(concatenate
                                                             'string
                                                             "-L" *libjvmdir*)
                                                            "-ljvm"
                                                            ,(concatenate
                                                              'string
                                                              "-Wl,-rpath," *libjvmdir*)))
                                 (let ((args (list "-shared" "-o" "libjffi.so"
                                                   (namestring (apply-output-translations "jffi_c_layer.o"))
                                                   (namestring (apply-output-translations "jffi_Lisp_start.o"))
                                                   (namestring (apply-output-translations "jffi_Java_pseudo_start.o"))
                                                   "libjffi_lisp.a" "-lecl"
                                                   (concatenate 'string "-L" *ecl-libdir*))))
                                   (if *ldflags*
                                       (setq args (append args (list *ldflags*))))
                                   (format t "~a ~a~%" c::*cc* args)
                                   (ext:run-program c::*cc*
                                                    args
                                                    :output t :error t))
                                 (uiop:delete-file-if-exists #p"jffi.o")
                                 (uiop:delete-file-if-exists #p"libjffi_lisp.a")
                                 (uiop:rename-file-overwriting-target #p"jffi.fas" (apply-output-translations #p"jffi.fas"))
                                 (uiop:rename-file-overwriting-target #p"libjffi.so" (apply-output-translations #p"libjffi.so")))
            :output-files (compile-op (o c)
                                      '("jffi.fas"
                                        "libjffi.so")))
     (:static-file "jffi_CLObject.h")
     (:static-file "jffi_Lisp.h")
     (:static-file "jffi_c_layer.h" :depends-on ("jffi_CLObject.h"
                                                 "jffi_Lisp.h"))
     (:c-source-file "jffi_c_layer" :depends-on ("jffi_c_layer.h")
                     :perform (compile-op (o c)
                                          (compile-c-file "jffi_c_layer.c"
                                                          (if (eql *pointersize* :32bit)
                                                              "-DINT_POINTER"
                                                              "-DLONG_POINTER"))))
     (:c-source-file "jffi_Java_start" :depends-on ("jffi_c_layer.h"))
     (:c-source-file "jffi_Java_pseudo_start" :depends-on ("jffi_c_layer.h"))
     (:c-source-file "jffi_Lisp_start" :depends-on ("jffi_c_layer.h"))
     (:java-package "java-code"
                    :pathname "jffi"
                    :components ((:static-file "CLObject.java.32bit")
                                 (:static-file "CLObject.java.64bit")
                                 (:java-source-file "CLObject" :depends-on ("CLObject.java.32bit" "CLObject.java.64bit")
                                                    :perform (compile-op :before (o c)
                                                                         (if (eql *pointersize* :32bit)
                                                                             (ext:copy-file #p"jffi/CLObject.java.32bit" #p"jffi/CLObject.java")
                                                                             (ext:copy-file #p"jffi/CLObject.java.64bit" #p"jffi/CLObject.java"))))
                                 ;; :perform (compile-op :after (o c)
                                 ;; 			 (uiop:delete-file-if-exists #p"jffi/CLObject.java")))
                                 (:java-source-file "LispEnvironmentError")
                                 (:java-source-file "LispCondition")
                                 (:java-source-file "Lisp"))))
    :perform (prepare-op (o s)
                         (uiop:chdir (component-pathname s))
                         ;; Try the defaults, useful for loading jffi as a dependency
                         (if (or (not *jvm-includedir*)
                                 (not *libjvmdir*))
                           (set-paths nil nil nil nil nil nil nil))))

(defun guess-directory (env-var-name path-spec)
  (let ((dir (si:getenv env-var-name)))
    (if dir
        (return-from guess-directory dir))
    (setq dir (first (directory path-spec)))
    (if dir
        (directory-namestring dir)
        nil)))

(defparameter *pointersize* 
  (cond
    ((or (find :i386 *features*)
         (find :i486 *features*)
         (find :i586 *features*)
         (find :i686 *features*)) :32bit)
    ((find :x86_64 *features*) :64bit)
    (t (format *error-output* "Unknown architecture, assuming 64 bit wide pointers!~%") :64bit)))
(defparameter *ecl-includedir* c::*ecl-include-directory*)
(defparameter *ecl-libdir* c::*ecl-library-directory*)
(defparameter *jvm-includedir* nil)
(defparameter *libjvmdir* nil)
(defparameter *cflags*
  (let ((envvar (si:getenv "CFLAGS")))
    (if envvar
        envvar
        "-O2")))
(defparameter *ldflags*
  (let ((envvar (si:getenv "LDFLAGS")))
    (if envvar
        envvar
        nil)))

(defun set-paths (pointersize ecl-includedir ecl-libdir jvm-includedir libjvmdir cflags ldflags)
  (if pointersize (setq *pointersize* pointersize))
  (if ecl-includedir (setq *ecl-includedir* ecl-includedir))
  (if ecl-libdir (setq *ecl-libdir* ecl-libdir))
  (if jvm-includedir
      (setq *jvm-includedir* jvm-includedir)
      (progn (setq *jvm-includedir* (guess-directory "JVM_INCLUDEDIR" "/usr/lib*/jvm/*/include/"))
             (if (not *jvm-includedir*)
                 (error "Guess for :jvm-includedir failed, please provide a value manually"))))
  (if libjvmdir
      (setq *libjvmdir* libjvmdir)
      (progn (setq *libjvmdir* (guess-directory "LIBJVMDIR" "/usr/lib*/jvm/*/jre/lib/*/server/"))
             (if (not *libjvmdir*)
                 (error "Guess for :libjvmdir failed, please provide a value manually"))))
  (if cflags (setq *cflags* cflags))
  (if ldflags (setq *ldflags* ldflags)))

;; Don't attempt to put this function in front of the system definition of jffi
;; if you're not a friend of stack overflows
(defmethod operate :before ((operation load-op) (component (eql (find-system "jffi"))) &rest initargs &key force force-not verbose (pointersize nil) (ecl-includedir nil) (ecl-libdir nil) (jvm-includedir nil) (libjvmdir nil) (cflags nil) (ldflags nil) &allow-other-keys)
  (set-paths pointersize ecl-includedir ecl-libdir jvm-includedir libjvmdir cflags ldflags))
(defmethod operate :before ((operation compile-op) (component (eql (find-system "jffi"))) &rest initargs &key force force-not verbose (pointersize nil) (ecl-includedir nil) (ecl-libdir nil) (jvm-includedir nil) (libjvmdir nil) (cflags nil) (ldflags nil) &allow-other-keys)
  (set-paths pointersize ecl-includedir ecl-libdir jvm-includedir libjvmdir cflags ldflags))

(defun compile-c-file (filename &optional extra-cflags &key (directory nil))
  (let ((args (list "-Wall" "-std=gnu99" "-fPIC" (concatenate 'string "-I" *ecl-includedir*)))
        (cwd (uiop:getcwd)))
    (if *cflags*
        (setq args (append args (list *cflags*))))
    (if extra-cflags
        (setq args (append args (list extra-cflags))))
    (setq args (append args (list (concatenate 'string "-I" *jvm-includedir*)
                                  (concatenate 'string "-I" *jvm-includedir* "/linux")
                                  "-c" filename)))
    (unwind-protect
         (progn (if directory
                    (uiop:chdir (merge-pathnames (make-pathname :directory (list :relative directory)) cwd)))
                (format t "~a ~a~%" c::*cc* args)
                (multiple-value-bind (stream return-value)
                    (ext:run-program c::*cc* args :output t :error t)
                  (declare (ignore stream))
                  (if (not (= return-value 0))
                      (error "Compilation of c file ~a failed" filename)
                      (let ((outputname (make-pathname :name (concatenate 'string (subseq filename 0 (- (length filename) 1)) "o"))))
                        (ensure-directories-exist (apply-output-translations outputname))
                        (uiop:rename-file-overwriting-target outputname (apply-output-translations outputname))))))
      (uiop:chdir cwd))))

(defun compile-java-file (directory filename &optional args)
  (if (equal directory "")
      (format t "javac ~a ~a~%" (if args args "") filename)
      (format t "javac ~a ~a/~a~%" (if args args "") directory filename))
  (multiple-value-bind (stream return-value)
      (ext:run-program
       "javac"
       (append (list
                "-cp" (concatenate 'string
                                   (directory-namestring (apply-output-translations ""))
                                   ":."))
               args
               (list (if (equal directory "")
                         filename
                         (concatenate 'string directory "/" filename))))
       :output t :error t)
    (declare (ignore stream))
    (if (not (= return-value 0))
        (error "Compilation of java file ~a failed" filename)
        (let ((outputname
               (make-pathname :directory (list :relative directory)
                              :name (concatenate 'string
                                                 (subseq filename 0 (- (length filename) 4))
                                                 "class"))))
          (ensure-directories-exist (apply-output-translations outputname))
          (uiop:rename-file-overwriting-target outputname (apply-output-translations outputname))
          (loop for file in (directory (make-pathname :directory (list :relative directory)
                                                      :name (concatenate 'string (subseq filename 0 (- (length filename) 5)) "*") :type "class"))
             do (uiop:rename-file-overwriting-target file (apply-output-translations file)))))))

(defun java-package-directory (comp)
  (let ((pcomp (component-parent comp)))
    (if (typep pcomp 'java-package)
        (directory-namestring (component-relative-pathname pcomp))
        "")))

(defmethod perform ((o load-op) (c c-source-file)))
(defmethod perform ((o compile-op) (c c-source-file))
  (compile-c-file (concatenate 'string (component-name c) ".c")))
(defmethod output-files ((o compile-op) (c c-source-file))
  (list (concatenate 'string (component-name c) ".o")))

(defmethod perform ((o load-op) (c java-source-file)))
(defmethod perform ((o compile-op) (c java-source-file))
  (compile-java-file (java-package-directory c)
                     (concatenate 'string (component-name c) ".java")))
;; Multiple class definitions in one file are not handled properly for output-files. They are copied anyway to the output directory.
(defmethod output-files ((o compile-op) (c java-source-file))
  (list (concatenate 'string (component-name c) ".class")))

(defsystem "jffi/test" 
    :depends-on ("jffi")
    :components
    ((:module "tests"
              :components
              ((:java-package
                "java-test-code"
                :pathname ""
                :components
                ((:java-source-file "test_class")
                 (:java-source-file "java_test"
                                    :perform (compile-op (o c)
                                                         (compile-java-file "" "java_test.java" '("-cp" ".:.."))))))
               (:c-source-file "lisp_init")
               (:file "lisp-test" :depends-on ("lisp_init")
                      :perform (compile-op (o c)
                                           (setq c::*ld-flags* *ldflags*
                                                 c::*cc-flags* (concatenate 'string
                                                                            *cflags*
                                                                            " -I" *jvm-includedir*
                                                                            " -I" *jvm-includedir* "/linux"
                                                                            " -fPIC")
                                                 c::*ecl-include-directory* *ecl-includedir*
                                                 c::*ecl-library-directory* *ecl-libdir*)
                                           (compile-file "lisp-test.lisp" :system-p t)
                                           (c:build-fasl "lisp-test" :lisp-files '("lisp-test.o"))
                                           (c:build-static-library "tests"
                                                                   :lisp-files '("lisp-test.o")
                                                                   :init-name "TESTS")
                                           (let ((args (list "-shared" "-o" "liblisp_init.so"
                                                             (namestring (apply-output-translations "lisp_init.o"))
                                                             (namestring (apply-output-translations "../libjffi.so"))
                                                             "libtests.a" "-lecl")))
                                             (if *ldflags*
                                                 (setq args (append args (list *ldflags*))))
                                             (format t "~a ~a~%" c::*cc* args)
                                             (ext:run-program c::*cc*
                                                              args
                                                              :output t :error t))
                                           (uiop:delete-file-if-exists #p"lisp-test.o")
                                           (uiop:delete-file-if-exists #p"libtests.a")
                                           (uiop:rename-file-overwriting-target #p"lisp-test.fas" (apply-output-translations #p"lisp-test.fas"))
                                           (uiop:rename-file-overwriting-target #p"liblisp_init.so" (apply-output-translations #p"liblisp_init.so")))
                      :output-files (compile-op (o c) '("lisp-test.fas" "liblisp_init.so"))))))
    :perform (prepare-op (o s)
                         (uiop:chdir (merge-pathnames #p"tests/" (component-pathname s))))
    :perform (test-op (o s)
                      (uiop:chdir (apply-output-translations ""))
                      (let ((args (list "-cp" ".:.." "-Djava.library.path=.:.."
                                        "-Xcheck:jni" "java_test")))
                        (format t "java ~a~%" args)
                        (multiple-value-bind (stream return-value)
                            (ext:run-program "java" args :output t :error t)
                          (declare (ignore stream))
                          (if (not (= return-value 0))
                              (error "Test failed!"))))))


