(setq c::*ld-flags* (si:getenv "LDFLAGS")
      c::*cc-flags* (si:getenv "CFLAGS")
      c::*ecl-include-directory* (si:getenv "ECL_INCLUDEDIR")
      c::*ecl-library-directory* (si:getenv "ECL_LIBDIR"))
(if (not
     (compile-file "jffi.lisp" :system-p t))
    (progn (format t "~%") (quit -1)))
(c:build-static-library "jffi_lisp"
                        :lisp-files '("jffi.o")
                        :init-name "JFFI_LISP")
