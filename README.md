JFFI is a fast, lightweight, bidirectional Java-Lisp foreign function
interface using Embeddable Common Lisp and the
Java Native Interface. It is designed to allow both embedding of Lisp
in Java programs and embedding of Java in Lisp programs.

For a small example how to build and use this library on Android see
https://gitlab.com/spaghettisalat/jffi_repl_example.
