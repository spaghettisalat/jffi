// Copyright 2017 Marius Gerbershagen
// This file is part of JFFI.
//
//  JFFI is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  JFFI is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with JFFI.  If not, see <http://www.gnu.org/licenses/>.

package jffi;

public class LispCondition extends RuntimeException{
	protected CLObject cond;
	public LispCondition(CLObject c){
		cond = c;
	}
	public CLObject getCondition(){
		return cond;
	}
	public String toString(){
		CLObject lstr = Lisp.toLispString("~a");
		CLObject fstr = Lisp.funcall("FORMAT", Lisp.NIL, lstr, cond);
		String ret = fstr.toString();
		fstr.unref();
		lstr.unref();
		return ret;
	}
}
